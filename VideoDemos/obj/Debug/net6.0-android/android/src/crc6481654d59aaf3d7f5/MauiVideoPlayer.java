package crc6481654d59aaf3d7f5;


public class MauiVideoPlayer
	extends androidx.coordinatorlayout.widget.CoordinatorLayout
	implements
		mono.android.IGCUserPeer,
		android.media.MediaPlayer.OnPreparedListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onPrepared:(Landroid/media/MediaPlayer;)V:GetOnPrepared_Landroid_media_MediaPlayer_Handler:Android.Media.MediaPlayer/IOnPreparedListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("VideoDemos.Platforms.Android.MauiVideoPlayer, VideoDemos", MauiVideoPlayer.class, __md_methods);
	}


	public MauiVideoPlayer (android.content.Context p0)
	{
		super (p0);
		if (getClass () == MauiVideoPlayer.class)
			mono.android.TypeManager.Activate ("VideoDemos.Platforms.Android.MauiVideoPlayer, VideoDemos", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}


	public MauiVideoPlayer (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == MauiVideoPlayer.class)
			mono.android.TypeManager.Activate ("VideoDemos.Platforms.Android.MauiVideoPlayer, VideoDemos", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android", this, new java.lang.Object[] { p0, p1 });
	}


	public MauiVideoPlayer (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == MauiVideoPlayer.class)
			mono.android.TypeManager.Activate ("VideoDemos.Platforms.Android.MauiVideoPlayer, VideoDemos", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, System.Private.CoreLib", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public void onPrepared (android.media.MediaPlayer p0)
	{
		n_onPrepared (p0);
	}

	private native void n_onPrepared (android.media.MediaPlayer p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
