﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrpcServer
{
    public class ReverseServer
    {
       
        private readonly Server _server;
        public ReverseServer()
        {
            _server = new Server()
            {
                Services = { RevService.Generated.RevService.BindService(new ReverseServiceImplementation()) },
                Ports = { new ServerPort("localhost", 11111, ServerCredentials.Insecure) }
            };
        }

        public void Start()
        { 
            _server.Start();
        }

        public async Task ShutDownAsync()
        { 
        await _server.ShutdownAsync();
        }

    }
}
