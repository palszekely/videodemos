﻿using Grpc.Core;
using RevService.Generated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrpcServer
{
    public class ReverseServiceImplementation : RevService.Generated.RevService.RevServiceBase
    {
        private Dictionary<String, String[]> versionDict = new Dictionary<String, String[]>()
        {
            {"v1.1", new string[] {"v1111", "v11111111","v111111111111" } },
            {"v2.2", new string[] {"v2222", "v22222222","v222222222222" } }
        };

        public override Task<HelloReply> Reverse(HelloRequest request, ServerCallContext context) 
        {
            /*var response = new Data() { Str = new string(request.Str.Reverse().ToArray()) };

            return Task.FromResult(response);*/
            List<string> versionIds = new List<string>();
            if (versionDict.ContainsKey(request.Name))
            {
                versionIds.AddRange(versionDict[request.Name]);
                List<RevService.Generated.Version> versions = new List<RevService.Generated.Version>();
                foreach (String versionNr in versionDict[request.Name])
                {
                    RevService.Generated.Version vv = new RevService.Generated.Version();
                    vv.VersionNr = versionNr;
                    versions.Add(vv);
                }
                HelloReply hr = new HelloReply
                {
                    Message = "Version " + versionDict[request.Name]
                };
                hr.VersionStrings.AddRange(versions);
                return Task.FromResult(hr);
            }
            else
            {
                return Task.FromResult(new HelloReply
                {
                    Message = "No version found"
                });
            }
        }
    }
}
