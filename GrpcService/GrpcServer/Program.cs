﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrpcServer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ReverseServer server = new ReverseServer();
            server.Start();
            Console.WriteLine("Server is listening...");

            Console.ReadKey();
        }
    }
}
